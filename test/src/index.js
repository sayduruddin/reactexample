import React from "react"
import ReactDOM  from "react-dom"

import App from "./components/App.js"

// This index.js file is my main point into the whole application. 
// This index.js file is imported into my index.html file which then builds everything from what it sees in here.
// So the hierarchy at this point is:


// The render method takes args (what do i want to render, where do i want to render it)
// ReactDOM.render(<MyInfo />, document.getElementById("root"))
// dont think I can use reactdom render twice for some reason yet
// ReactDOM.render(<MyContent />, document.getElementById("content"))

ReactDOM.render(<App />, document.getElementById("root"))
