import React from "react"

// the function MyApp is a react functional component, it looks like html but is JSX.
// Again, everything must be contained in one element, in this case, one div element
// As we create more and more components, we will split them into their own files for better readability.
function MyInfo() {
    return (
    <div>
        <h1>Saydur Uddin</h1>
        <p>This is a paragraph about me</p>
        <ul>
            <li>Computer science</li>
            <li>Cyber security</li>
            <li>My third item</li>
        </ul>
    </div>
    )
}

export default MyInfo