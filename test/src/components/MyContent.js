import React from "react";

function MyContent() {
    return(
        <div>
            <h2>
                This should be h2 inside the content container.
            </h2>
            <h3>
                This should be h3 inside the content container.
            </h3>
        </div>
    )

}

export default MyContent